from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor
from twisted.application.internet import MulticastServer
import sys

class MulticastClientUDP(DatagramProtocol):
	messageList = ["foo:$10", "bar:$30", "foo:$20", "bar:$20", "foo:$30", "bar:$10"]
	messageCounter = 0
	nodeAddress = ('224.0.0.1', int(sys.argv[1]))

	def datagramReceived(self, datagram, address):
		print("Received message: " + repr(datagram.decode()) + " from node " + str(address))
		if self.messageCounter < 5:
			self.messageCounter += 1
			self.transport.write(self.messageList[self.messageCounter].encode(), self.nodeAddress)


# Send multicast on 224.0.0.1:(insert port number), on our dynamically allocated port
reactor.listenUDP(0, MulticastClientUDP()).write(b"foo:$10", ('224.0.0.1', int(sys.argv[1])))
reactor.run()