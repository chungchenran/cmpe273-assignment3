from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor
from twisted.application.internet import MulticastServer
import sys, re, pickledb
import collections

nodeIds = ["3000", "3001", "3002", "3003"]

# load a database with pickledb
db = pickledb.load('assignment3_' + str(sys.argv[1]) + '.db', False)
db.dump()

class MulticastServerUDP(DatagramProtocol):
	currentNodeId = None 	# Save the node id
	currentQuorum = None 	# Save the quorum for this node
	numVotesFor = 0			# The number of votes for a datagram
	clientAddress = None 	# Save the client address
	nextNode = None 		# Save the next node in the node list

	def __init__(self):
		self.currentNodeId = str(sys.argv[1])
		if int(sys.argv[1]) < 3003:
			self.nextNode = int(sys.argv[1]) + 1
		else:
			self.nextNode = 0	# when we reach the last node
		self.currentQuorum = dict()
		for name in nodeIds:
			if name != self.currentNodeId:
				self.currentQuorum[name] = ('224.0.0.1', int(name))	# addresses of quorum nodes

	def startProtocol(self):
		print('Started Listening')
		# Join a specific multicast group, which is the IP we will respond to
		self.transport.joinGroup('224.0.0.1')

	def datagramReceived(self, datagram, address):
		if re.match('^([a-z]{3}):(\$[0-9]+)$', datagram.decode()):		# for data
			print("Received message from client of address " + str(address))
			print("Datagram = " + datagram.decode())
			self.clientAddress = address 	# save the client's address
			ballotSignal = "RequestBallot<" + datagram.decode() + ">"
			self.beginVotingProcess(ballotSignal.encode())	# replicate data to nodes in quorum
		elif re.match('^RequestBallot<(.+?):(.+?)>$', datagram.decode()):	# for voting
			m1 = re.search('^RequestBallot<(.+?)>$', datagram.decode())
			nodeBallot = "Vote=Yes<" + m1.group(1) + ">"
			self.transport.write(nodeBallot.encode(), address)	# assume accepting for now
		elif re.match('^Vote=Yes<(.+?):(.+?)>$', datagram.decode()):	# receive vote
			self.numVotesFor += 1
			if self.numVotesFor == len(self.currentQuorum):
				self.numVotesFor = 0		# reset counter to 0
				m2 = re.search('^Vote=Yes<(.+?)>$', datagram.decode())
				self.updateDB(m2.group(1))
		else:
			return

	def beginVotingProcess(self, datagram):
		for node in self.currentQuorum:
			print("Sending ballot request to node %s" % node)
			print("Datagram = " + datagram.decode())
			self.transport.write(datagram, self.currentQuorum[node])

	def moveToNextNode(self, datagram):
		if self.nextNode != 0:
			print("Moving to node %d" % self.nextNode)
			self.transport.write(datagram, ('224.0.0.1', self.nextNode))
		else:
			print("Reset to node 3000 with new data")
			self.transport.write(b"start with new data", self.clientAddress)

	def updateDB(self, kv_pair):
		m = re.search('^(.+?):(.+?)$', kv_pair)		# group the key and the value chars
		db.set(m.group(1), m.group(2))				# set the key-value pair in pickledb
		keyList = db.getall()						# retrieve list of all keys in pickledb
		print("Printing out all key-value pairs in database...")
		for key in keyList:
			print("key = " + key + ", value = " + db.get(key))	# list all key-value pairs in pickledb
		db.dump()		# save the changes to the db file
		self.transport.write(b"Data received", self.clientAddress)	# send reply message to client
		self.moveToNextNode(kv_pair.encode())


# Listen for multicast on 224.0.0.1:(port number)
reactor.listenMulticast(int(sys.argv[1]), MulticastServerUDP())
reactor.run()